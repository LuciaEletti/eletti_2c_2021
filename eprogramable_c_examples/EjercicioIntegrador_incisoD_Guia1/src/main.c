/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include "stdint.h"

/*==================[macros and definitions]=================================*/
#define CANTIDAD_BITS 4

typedef struct{

	uint8_t port;
	uint8_t pin;
	uint8_t dir;
}gpioConf_t;

gpioConf_t vector[4]={{1,4,1},{1,5,1},{1,6,1},{2,14,1}};

uint8_t numero_en_BCD;

uint8_t i;

uint8_t variable_aux;

void Funcion7Seg (uint8_t num_BCD, gpioConf_t arreglo[4]);

/*==================[internal functions declaration]=========================*/

void Funcion7Seg (uint8_t num_BCD, gpioConf_t arreglo[4]){

	for(i=0; i<CANTIDAD_BITS; i++){

		variable_aux= num_BCD & (0x01);
		printf("Se prendio el led: %d\n\r", i+1);

		printf("Puerto: %d\n\r", arreglo[i].port);

		printf("Pin: %d\n\r", arreglo[i].pin);

		printf("Direccion: %d\n\r", arreglo[i].dir);

		num_BCD=num_BCD>>1;
	}

}


int main(void)
{
	numero_en_BCD=9;
	Funcion7Seg(numero_en_BCD,vector);

	return 0;
}

/*==================[end of file]============================================*/

