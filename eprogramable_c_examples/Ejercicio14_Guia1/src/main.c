/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include "stdint.h"
#include "string.h"

/*==================[macros and definitions]=================================*/

struct alumno{

	char nombre[12];
	char apellido[20];
	uint8_t edad;
}alumno_1,*alumno_2Ptr, alumno_2;

/*==================[internal functions declaration]=========================*/

int main(void)
{

  alumno_2Ptr=&alumno_2;

  strcpy(alumno_1.nombre,"Lucia");

  strcpy(alumno_1.apellido,"Eletti");

  alumno_1.edad=23;

  printf("Nombre: %s\n\r",alumno_1.nombre);
  printf("Apellido: %s\n\r",alumno_1.apellido);
  printf("Edad: %d\n\r",alumno_1.edad);

 strcpy(alumno_2Ptr->nombre,"Clementina");

 strcpy(alumno_2Ptr->apellido,"Muñoz");

  alumno_2Ptr->edad=21;

  printf("Nombre: %s\n\r",alumno_2Ptr->nombre);
  printf("Apellido: %s\n\r",alumno_2Ptr->apellido);
  printf("Edad: %d\n\r",alumno_2Ptr->edad);


	return 0;
}

/*==================[end of file]============================================*/

