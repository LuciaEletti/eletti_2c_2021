/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include "stdint.h"
#include "string.h"
/*==================[macros and definitions]=================================*/

union corrimiento{
	struct corrimiento2{
	uint8_t A2;
	uint8_t B2;
	uint8_t C2;
	uint8_t D2;

}nombrestruct;

 uint32_t V2;

}nombreunion;

/*==================[internal functions declaration]=========================*/

int main(void)

{
   uint32_t V=0x01020304;


   uint8_t A;
   uint8_t B;
   uint8_t C;
   uint8_t D;

   A=V;

   B=(V>>8);
   C=(V>>16);
   D=(V>>24);

   printf("A: %d\n\r",A);
   printf("B: %d\n\r",B);
   printf("C: %d\n\r",C);
   printf("D: %d\n\r",D);


   nombreunion.V2=0x01020304;
   nombreunion.nombrestruct.A2=nombreunion.V2;
   nombreunion.nombrestruct.B2=(nombreunion.V2>>8);
   nombreunion.nombrestruct.C2=(nombreunion.V2>>16);
   nombreunion.nombrestruct.D2=(nombreunion.V2>>24);


   printf("A2: %d\n\r",nombreunion.nombrestruct.A2);
   printf("B2: %d\n\r",nombreunion.nombrestruct.B2);
   printf("C2: %d\n\r",nombreunion.nombrestruct.C2);
   printf("D2: %d\n\r",nombreunion.nombrestruct.D2);



	return 0;
}

/*==================[end of file]============================================*/

