/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include "stdint.h"

/*==================[macros and definitions]=================================*/

struct leds
{
	uint8_t n_led;        //indica el número de led a controlar
	uint8_t n_ciclos;   //indica la cantidad de ciclos de encendido/apagado
	uint8_t periodo;   // indica el tiempo de cada ciclo
	uint8_t modo;      // ON, OFF, TOGGLE

} my_leds;

#define  ON 1
#define  OFF 2
#define  TOGGLE 3


void LedControl (struct leds *puntero);


/*==================[internal functions declaration]=========================*/

void LedControl (struct leds *puntero){

	puntero=&my_leds;

	switch(puntero->modo){

	case 1:
	{
		if(puntero->n_led==1){
		printf("Se prende led 1 \n\r");}

		else if(puntero->n_led==2){
				printf("Se prende led 2 \n\r");}

	   else if(puntero->n_led==3){
			printf("Se prende led 3 \n\r");}

	}
	break;

	case 2:{

	if(puntero->n_led==1){
	printf("Se apaga led 1 \n\r");}

	else if(puntero->n_led==2){
    printf("Se apaga led 2 \n\r");}

    else if(puntero->n_led==3){
    printf("Se apaga led 3 \n\r");}

			}
			break;

	case 3:{
	uint8_t i;
	for(i=0;i<puntero->n_ciclos;i++){
		if(puntero->n_led==1){
	   printf("Espera led 1 \n\r");}

		else if(puntero->n_led==2){
	    printf("Espera led 2 \n\r");}

	    else if(puntero->n_led==3){
	    printf("Espera led 3 \n\r");}
	}
	}
	break;

}

}
int main(void)
{
	my_leds.modo=TOGGLE;
	my_leds.n_led=1;
	my_leds.n_ciclos=5;

	LedControl(&my_leds);

	return 0;
}

/*==================[end of file]============================================*/

