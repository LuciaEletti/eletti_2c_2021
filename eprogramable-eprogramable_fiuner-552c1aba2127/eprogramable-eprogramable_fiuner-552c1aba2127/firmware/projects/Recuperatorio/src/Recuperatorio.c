/*! @mainpage Recuperatorio
 *
 * \section genDesc General Description
 *
 *
 *
 * \section hardConn Hardware Connection
 *
 * |   Infrarrojo	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	DOUT	 	| 	 T_COL0	    |
 * | 	+5V 	 	| 	  +5V    	|
 * | 	GND	    	| 	  GND		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 13/09/2021 | Document creation		                         |
 * | 11/11/2021 | The application is modified by Lucía Eletti|
 * | 			| 	                     						 |
 *
 * @author Lucía Eletti
 *

/*==================[inclusions]=============================================*/
#include "../inc/Recuperatorio.h"       /* <= own header */

#include "DisplayITS_E0803.h"
#include "gpio.h"
#include "systemclock.h"
#include "Tcrt5000.h"
#include "delay.h"
#include "led.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
/*==================[macros and definitions]=================================*/

#define PESOMAX 150
#define VMAX 3.3
#define PESOTOLERABLE 20
#define CICLODETRABAJO 1000
#define LOTECOMPLETO 15

/*==================[internal data definition]===============================*/

bool encender=false;

bool detener;

uint8_t tecla_recibida;

float dato_leido=0;//dato que ingresa

float peso=0;//almacena el peso

bool cinta_uno=false;//booleano para cinta 1

bool cinta_dos=false;// booleano para cinta 2

uint16_t cantidad_cajas_llenadas=0;

uint16_t tiempo=0;

uint16_t tiempo_en_seg=0;

uint16_t menor_tiempo;//guardo menor tiempo

uint16_t mayor_tiempo;//guardo mayor tiempo

/*==================[internal functions declaration]=========================*/

/** @fn float VoltajeAPeso();
 * @brief convierte voltaje a peso
 * @param[in]
 * @return el peso
 */

float VoltajeAPeso(void);//convierte voltaje a peso

/** @fn void doTimer(void);
 * @brief maneja el funcionamiento del programa
 * @param[in]
 * @return
 */
void doTimer(void);//controla funcionamineto de las cintas


/** @fn void doSwitch1(void);
 * @brief cambia estado telca 1
 * @param[in]
 * @return
 */
void doSwitch1(void);//maneja tecla 1 para encender

/** @fn void doSwitch2(void);
 * @brief cambia estado telca 2
 * @param[in]
 * @return
 */
void doSwitch2(void);//maneja tecla 2 para apagar

/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

float VoltajeAPeso(float dato){

	dato = BalanzaConversion();
	valor_peso = dato*VMAX/PESOMAX;
	return valor_peso;

}

void doTimer(void){

	if(encender==true){
	if(cinta_uno==true){
		LedOn(LED_1);//enciendo cinta1
	if(Tcrt5000State()==true){// las cajas vacias supongo valen 0kg,
		//por ende cuando se carguen con penso me devuelve verdadero
		LedOff(LED_1);// apago cinta1
		cinta_dos=true;
	}}

	if(cinta_dos==true){

		LedOn(LED_2);//arranca cinta 2

	tiempo=tiempo+CICLODETRABAJO;
	dato_leido = BalanzaConversion();
	peso = VoltajeAPeso(dato_leido);
	//aumento la cantidad de objeto por cada vez que pasa aca

	if(peso>PESOTOLERABLE){// si el peso ya supera los 20kg se apaga cinta 2 y arranca cinta1

		cantidad_cajas_llenadas++;

		tiempo_en_seg=tiempo/CICLODETRABAJO;

	UartSendString(SERIAL_PORT_PC, "Tiempo de llenado de caja");
	UartSendString(SERIAL_PORT_PC, UartItoa(cantidad_objetos, 10));
	UartSendString(SERIAL_PORT_PC, UartItoa(tiempo_en_seg, 10));
	UartSendString(SERIAL_PORT_PC, " seg\r\n");

	if(tiempo_en_seg<menor_tiempo){

		menor_tiempo=tiempo_en_seg;//guardo menor tiempo
	}
	if(tiempo_en_seg>mayor_tiempo){

			mayor_tiempo=tiempo_en_seg;//guardo mayor tiempo
		}

	if(cantidad_cajas_llenadas==LOTECOMPLETO){

		UartSendString(SERIAL_PORT_PC, "Tiempo de llenado maximo");
		UartSendString(SERIAL_PORT_PC, UartItoa(mayor_tiempo, 10));
		UartSendString(SERIAL_PORT_PC, "Tiempo de llenado minimo");
		UartSendString(SERIAL_PORT_PC, UartItoa(menor_tiempo, 10));
		UartSendString(SERIAL_PORT_PC, " seg\r\n");
		mayor_tiempo=0;
		menor_tiempo=0;
	}

	LedOff(LED_2);
			cinta_uno=true;//pongo en verdadero bandera cinta1
			cinta_dos=false;
	}

	}
}

}


void doSwitch1(void){
	encender=true;
}


void doSwitch2(void){
	detener=true;
}


int main(void){

	SystemClockInit();
	LedsInit();
	SwitchesInit();
	Tcrt5000Init(GPIO_T_COL0);

	SwitchActivInt(1, *doSwitch1);
	SwitchActivInt(2, *doSwitch2);

	timer_config my_timer = {TIMER_A,CICLODETRABAJO,&doTimer};
	TimerInit(&my_timer);
	TimerStart(TIMER_A);

	serial_config my_uart = {SERIAL_PORT_PC,115200,NULL};
	UartInit(&my_uart);

	while(1){


	}

	return 0;

}




/*==================[end of file]============================================*/

