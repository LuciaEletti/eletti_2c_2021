﻿Descripción del Proyecto.

El proyecto se trata sobre la adquisición, procesamiento y visualización de una señal de EMG para el analisis
de una protesis de mano.
La señal es adquirida mediante electrodos ubicados en el antebrazo. Se le pide a la persona que deje el brazo
completamente relajado, luego que lo cierre lentamente y por ultimo que haga la mayor fuerza posible cerrando 
el puño. 
Estos cambios en la señal son registrados y en base a estos valores es posible mover un servomotor.
Esto nos sirve como guia, ya que el servo rotará un determinado ángulo en función del valor de fuerza que se realice.
Tiene un angulo maximo y un angulo minimo que puede rotar, por ende nos permite identificar si la persona logro 
realizar una fuerza maxima dentro del promedio, como es el estado del puño relajado, y varios movimientos
que el medico crea necesario para analizar el funcionamiento.
Tambien se puede visulizar la señal mediante la app Serial Ploter, y se puede observar tanto el EMG de entrada
como la señal procesada. 