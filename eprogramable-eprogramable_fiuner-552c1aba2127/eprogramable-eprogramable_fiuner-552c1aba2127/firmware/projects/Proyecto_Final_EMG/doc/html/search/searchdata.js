var indexSectionsWithContent =
{
  0: "abcdefmoprsv",
  1: "p",
  2: "cfmr",
  3: "abers",
  4: "adefopv",
  5: "p"
};

var indexSectionNames =
{
  0: "all",
  1: "files",
  2: "functions",
  3: "variables",
  4: "defines",
  5: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Files",
  2: "Functions",
  3: "Variables",
  4: "Macros",
  5: "Pages"
};

