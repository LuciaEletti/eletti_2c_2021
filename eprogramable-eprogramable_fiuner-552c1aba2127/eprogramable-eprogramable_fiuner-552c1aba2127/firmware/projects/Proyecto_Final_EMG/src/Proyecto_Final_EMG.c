/*! @mainpage Proyecto final:
 *  Adquisición, procesamiento y visualización de una señal de EMG para el analisis
 *  de un protesis de mano.
 *
 *
 * \section genDesc General Description
 *
 * La aplicación permite el control de una protesis de mano,
 * a traves del estudio de la señal de EMG del antebrazo.
 * Nos permite saber cuando el puño se encuentra relajado, haciendo una fuerza media
 * o cuando se contrae lo más que se pueda cerrando el puño. Esto provocara la rotación de
 * un servomotor, el cual tiene un angulo minimo y un angulo maximo de rotacion, y dentro de
 * esos valores rotara en funcion del valor de la fuerza.
 *
 * \section hardConn Hardware Connection
 *
 * | PLACA ADQ. EMG |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |   J1 canal 2 	| 	   CH1		|
 * |   	  VCC	 	| 	  +5V		|
 * |   	  GND	 	| 	   GND		|
 *
 * *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 15/11/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Eletti Lucía
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto_Final_EMG.h"
#include "systemclock.h"
#include "bool.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include "switch.h"
#include "pwm_sct.h"


/*==================[macros and definitions]=================================*/

/** @def FC
 * @brief Valor de la frecuencia de corte para el filtro pasa bajos.
 */
#define FC 3
/** @def dt
 * @brief Tiempo a la que se muestrea la señal de entrada.
 */
#define dt 0.002
/** @def PI
 * @brief Valor de la constante PI.
 */
#define PI 3.14
/** @def VC
 * @brief Valor de la componente de continua que debe eliminarse con la rectificación.
 */
#define VC 475
/** @def ESFUERZOMEDIO
 * @brief Valor del esfuerzo medio.
 */
#define ESFUERZOMEDIO 800
/** @def ANGULOESFUERZOMAX
 * @brief Valor maximo que puede rotar el servo y se corresponde con el esfuerzo maximo.
 */
#define ANGULOESFUERZOMAX 90
/** @def ANGULOESFUERZOMIN
 * @brief Valor minimo que puede rotar el servo y se corresponde con el esfuerzo minimo.
 */
#define ANGULOESFUERZOMIN -90
/** @def ANGULOESFUERZOMED
 * @brief Valor medio que puede rotar el servo y se corresponde con el esfuerzo medio.
 */
#define ANGULOESFUERZOMED 0
/** @def ORDENADAALORIGEN
 * @brief Valor de la ordenada al origen de la ecuacion de la recta para relacionar fuerza y angulos.
 */
#define ORDENADAALORIGEN -114
/** @def PENDIENTE
 * @brief Valor de la pendiente de la ecuacion de la recta para relacionar fuerza y angulos.
 */
#define PENDIENTE 0.12

/* booleano para detectar si comenzo la conversion A-D*/
bool bandera_para_ADC = false;

/* variable donde se guarda el dato de entrada correspondiente a un valor analogico*/
uint16_t entrada_valor_analogico=0;

/* variable donde se guardara el valor de entrada menos la componente de continua*/
int16_t entrada_rectificada=0;

/* variable donde se guardara el valor de entrada despues de la rectificacion*/
uint16_t entrada_rectificada_al_cuadrado=0;

/* variable donde se guardara el valor de la salida despues del filtro pasa bajos*/
uint16_t salida_filtrada=0;

/* variable donde se guardara el valor de la salida filtrada anterior para la ecuacion del
 * filtro pasa bajos
 */
uint16_t salida_filtrada_anterior=0;

/* variable que almacena el valor del angulo a moverse, el cual se calcula con la ecuacion de la recta*/
int16_t angulo_a_moverse=0;

/* variable float donde se almacenará el valor del parametro alfa para la ecuacion del filtro*/
float alfa=0;

/* variable float donde se guardara el valor de RC*/
float RC=0;

/*==================[internal functions declaration]=========================*/

/** @fn void FuncionComienzoADC(void)
 * @brief Dispara la conversión analogica - digital
 * @param[in]
 * @return
 */
void FuncionComienzoADC(void);

/** @fn void FuncionAnalog(void)
 * @brief Lee un dato analogico de entrada
 * @param[in]
 * @return
 */
void FuncionAnalog(void);

/** @fn void Rectificacion(void)
 * @brief Rectifica la señal de entrada. Elimina la componente de continua.
 * @param[in]
 * @return
 */
void Rectificacion(void);

/** @fn void CalculoParametroAlfa(void)
 * @brief Calcula el parametro alfa que pertenece a la ecuación de filtrado pasa bajos
 * @param[in]
 * @return
 */
void CalculoParametroAlfa(void);

/** @fn void FiltroPasaBajos(void)
 * @brief Filtra la señal de entrada. Atenua las frecuencias que se encuentren en un valor
 * por encima de 3 Hz y permite el paso de las frecuencias menores a FC.
 * @param[in]
 * @return
 */
void FiltroPasaBajos(void);

/** @fn void ConversionFuerzaAngulo(void)
 * @brief Realiza una conversion entre el valor de la fuerza que realiza la persona
 * y el ángulo que se debe mover el servo en funcion de la misma.
 * @param[in]
 * @return
 */
void ConversionFuerzaAngulo(void);

/*==================[external functions definition]==========================*/

void FuncionComienzoADC(void) {

	AnalogStartConvertion();
}

void FuncionAnalog(void) {

	AnalogInputRead(CH1, &entrada_valor_analogico);
	bandera_para_ADC = true;
}

void Rectificacion(void){

	entrada_rectificada = entrada_valor_analogico - VC;
	entrada_rectificada_al_cuadrado = entrada_rectificada * entrada_rectificada;
}

void CalculoParametroAlfa(void){

	RC = 1/(2*PI*FC);
	alfa = dt / (RC + dt);
}

void FiltroPasaBajos(void){

salida_filtrada = salida_filtrada_anterior + alfa * (entrada_rectificada_al_cuadrado - salida_filtrada_anterior);
salida_filtrada_anterior = salida_filtrada;

}

void ConversionFuerzaAngulo(void){

	angulo_a_moverse = ORDENADAALORIGEN + (PENDIENTE * salida_filtrada);

	if (angulo_a_moverse < ANGULOESFUERZOMIN) angulo_a_moverse= ANGULOESFUERZOMIN;

	if (angulo_a_moverse > ANGULOESFUERZOMAX) angulo_a_moverse=ANGULOESFUERZOMAX;

    ServoOn(CTOUT0,angulo_a_moverse);

}

int main(void){

	//Inicialización del Siystem Clock
	SystemClockInit();

    //elijo timer A porque tiene el tiempo en 1 ms, y un 2 para que sea 2ms, entonces 1/2ms me da 500Hz=fm
	timer_config my_timer = {TIMER_A, 2, FuncionComienzoADC};
	//inicializo timer
	TimerInit(&my_timer);
	TimerStart(TIMER_A);

	analog_input_config my_analog = {CH1, AINPUTS_SINGLE_READ, FuncionAnalog};
	AnalogInputInit(&my_analog);
	AnalogOutputInit();

	//inicializo la uart e indico puerto
    serial_config my_uart = {SERIAL_PORT_PC,115200,NULL};
    UartInit(&my_uart);

    //Calculo parametros para el filtro
    CalculoParametroAlfa();

    //Inicializo el servo
    ServoInit(CTOUT0);

	while(1){

	if(bandera_para_ADC == true){

	Rectificacion();
	FiltroPasaBajos();

	UartSendString(SERIAL_PORT_PC, UartItoa(entrada_rectificada_al_cuadrado,10));
	UartSendString(SERIAL_PORT_PC, ",");

	UartSendString(SERIAL_PORT_PC, UartItoa(salida_filtrada,10));
	UartSendString(SERIAL_PORT_PC, "\r\n");

	ConversionFuerzaAngulo();

    bandera_para_ADC=false; }

	}

	return 0;
}

/*==================[end of file]============================================*/

