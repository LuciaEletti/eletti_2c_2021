/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "../../Proyecto3/inc/template.h"

#include "hc_sr4.h"
#include "gpio.h"
#include "switch.h"
#include "DisplayITS_E0803.h"
#include "timer.h"
#include "uart.h"


/*==================[macros and definitions]=================================*/

uint16_t a=1;

uint16_t valor_distancia_prueba_en_cm;

uint16_t valor_distancia_prueba_en_plg;

uint8_t teclas;

uint8_t dato_recibido;

uint8_t dato_convertido;

uint8_t tecla1_on_off;

uint8_t tecla2_hold;

gpio_t pines[7]= {GPIO_LCD_1, GPIO_LCD_2, GPIO_LCD_3, GPIO_LCD_4, GPIO_1, GPIO_3, GPIO_5};

/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/

void doSwitch1 (void);

void doSwitch2(void);

void doTimer(void);

void doUart(void);

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

void doSwitch1 (void){
	tecla1_on_off=!tecla1_on_off;
}

void doSwitch2(void){
	tecla2_hold=!tecla2_hold;
}

void doTimer(void){

	if(tecla1_on_off==true){

			valor_distancia_prueba_en_cm = HcSr04ReadDistanceCentimeters();

		if(tecla2_hold==false){

		if (valor_distancia_prueba_en_cm > 0 && valor_distancia_prueba_en_cm <= 10) {
						LedOn(LED_RGB_B);
						LedOff(LED_1);
						LedOff(LED_2);
						LedOff(LED_3);
						}
		if (valor_distancia_prueba_en_cm > 10 && valor_distancia_prueba_en_cm <= 20) {
						LedOn(LED_RGB_B);
						LedOn(LED_1);
						LedOff(LED_2);
						LedOff(LED_3);
						}
		if (valor_distancia_prueba_en_cm > 20 && valor_distancia_prueba_en_cm <= 30) {
						LedOn(LED_RGB_B);
						LedOn(LED_1);
						LedOn(LED_2);
						LedOff(LED_3);
						}
		if (valor_distancia_prueba_en_cm > 30) {
						LedOn(LED_RGB_B);
						LedOn(LED_1);
						LedOn(LED_2);
						LedOn(LED_3);
						}

		ITSE0803DisplayValue(valor_distancia_prueba_en_cm);

		//dato_convertido = UartItoa(valor_distancia_prueba_en_cm, base);

		UartSendString(SERIAL_PORT_PC, UartItoa(valor_distancia_prueba_en_cm, 10));
		UartSendString(SERIAL_PORT_PC, " cm\r\n");

		}

		if(tecla2_hold==true){}

		}
		if(tecla1_on_off==false){
			LedOff(LED_RGB_B);
			LedOff(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);
		}
}


void doUart(void){

    UartReadByte(SERIAL_PORT_PC,&dato_recibido);

	if(dato_recibido=='O'){
		tecla1_on_off=!tecla1_on_off;
	}
	if(dato_recibido=='H'){
		tecla2_hold=!tecla2_hold;
		}
}


int main(void){


	LedsInit();
	SwitchesInit();
    HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);
    SystemClockInit();
    ITSE0803Init(pines);

    SwitchActivInt(1, *doSwitch1);

    SwitchActivInt(2, *doSwitch2);

    timer_config my_timer = {TIMER_A,1000,&doTimer};

    serial_config my_uart = {SERIAL_PORT_PC,115200,&doUart};

    TimerInit(&my_timer);
    TimerStart(TIMER_A);

    UartInit(&my_uart);

	while(a==1){

	}

	return 0;
}

/*==================[end of file]============================================*/

