/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Eletti Lucía
 */

/*==================[inclusions]=============================================*/
#include "../inc/Examen.h"

#include "systemclock.h"
#include "bool.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include "switch.h"
#include "hc_sr4.h"
#include "goniometro.h"


/*==================[macros and definitions]=================================*/

#define A0 0
#define A1 15
#define A2 30
#define A3 45
#define A4 60
#define A5 75


/*==================[internal data definition]===============================*/

uint32_t valor_distancia;

uint32_t distancia_anterior;

uint32_t angulo;

uint32_t nuevo_angulo;

/*==================[internal functions declaration]=========================*/
/** @fn void MedicionDistancia
 * @brief Lee y almacena valor de distancia segun el angulo
 * @param[in]
 * @return
 */
void MedicionDistancia(void);

/** @fn void doTimer
 * @brief Desarrollo de la aplicacion. Relaciona angulos con distancia leida por el sensor.
 * @param[in]
 * @return
 */
void doTimer(void);

/** @fn void EncenderLeds
 * @brief Enciende los leds dependiendo para donde se mueva el sensor.
 * @param[in]
 * @return
 */
void EncenderLeds(void);


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/


void MedicionDistancia(){

	angulo =  GoniometroRead();

	switch (angulo){

			case (A0):
				valor_distancia =HcSr04ReadDistanceCentimeters();
			case (A1):
				valor_distancia =HcSr04ReadDistanceCentimeters();
			case (A2):
				valor_distancia =HcSr04ReadDistanceCentimeters();
			case (A3):
				valor_distancia =HcSr04ReadDistanceCentimeters();
			case (A4):
				valor_distancia =HcSr04ReadDistanceCentimeters();
			case (A5):
				valor_distancia =HcSr04ReadDistanceCentimeters();
}
	}


void doTimer(void){

	distancia_anterior = valor_distancia;

	nuevo_angulo = GoniometroRead();

	MedicionDistancia();

	if(nuevo_angulo != angulo){

	angulo=nuevo_angulo;

	MedicionDistancia();

	UartSendString(SERIAL_PORT_PC, UartItoa(angulo,10));
	UartSendString(SERIAL_PORT_PC, "° objeto a ");
	UartSendString(SERIAL_PORT_PC, UartItoa(valor_distancia,10));
	UartSendString(SERIAL_PORT_PC, " cm\r\n");

	}

	if(valor_distancia<distancia_anterior){

	UartSendString(SERIAL_PORT_PC, "Obstaculo en ");
	UartSendString(SERIAL_PORT_PC, UartItoa(angulo,10));
	UartSendString(SERIAL_PORT_PC, "°\r\n");

	}


}

void EncenderLeds(){

	if(nuevo_angulo>angulo){
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);
}
	if(nuevo_angulo<angulo){
		LedOn(LED_RGB_B);
		LedOff(LED_1);
		LedOn(LED_2);
		LedOff(LED_3);
	}

	}

int main(void){

	SystemClockInit();
	HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);
	LedsInit();

	MedicionDistancia();
	timer_config my_timer = {TIMER_A,1000,&doTimer};
	TimerInit(&my_timer);
	TimerStart(TIMER_A);

	EncenderLeds();

	serial_config my_uart = {SERIAL_PORT_PC,115200,NULL};
	UartInit(&my_uart);

	analog_input_config my_analog = {CH1, AINPUTS_SINGLE_READ,GoniometroStart()};
	GoniometroInit(&my_analog);


	while(1){

	}


	return 0;
}

/*==================[end of file]============================================*/

